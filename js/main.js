// Create a "close" button and append it to each list item
var myNodeList = document.getElementsByTagName("LI");
var i;
for (i = 0; i < myNodeList.length; i++) {
    var span = document.createElement("SPAN");
    var txt = document.createTextNode("\u00D7");
    span.className = "close";
    span.appendChild(txt);
    myNodeList[i].appendChild(span);
}

// Click on a close button to hide the current list item
var close = document.getElementsByClassName("close");
var i;
for (i = 0; i < close.length; i++) {
    close[i].onclick = function () {
        var div = this.parentElement;
        div.style.display = "none";
    }
}

// // Add a "checked" symbol when clicking on a list item
// var list = document.querySelector('ul');
// list.addEventListener('click', function(ev) {
//   if (ev.target.tagName === 'LI') {
//     ev.target.classList.toggle('checked');
//   }
// }, false);

// Create a new list item when clicking on the "Add" button
function add_ingredient_to_recipe() {
    var li = document.createElement("li");
    li.setAttribute("style", "column-count: 4; ");
    var text = [];

    var new_recipe = document.getElementById("recipe_name").value;
    var new_ingredient = document.getElementById("new_ingredient").value;
    var new_amount = document.getElementById("new_amount").value;
    var new_unit = document.getElementById("new_unit").value;
    var new_note = document.getElementById("new_note").value;

    var sp = document.createTextNode(". ");

    if (new_recipe === '') {
        alert("Please enter recipe name!");
    } else if (new_ingredient === '') {
        alert("Please enter ingredient!");
    } else if (new_amount === '') {
        alert("Please enter amount!");
    } else if (new_unit === '') {
        alert("Please enter units of measure!");
    } else {
        text.push(document.createTextNode(new_amount+ " \t"));
        text.push(document.createTextNode(new_unit+ "\t"));
        text.push(document.createTextNode(new_ingredient + ",\t"));
        if (new_note !== '') {
            text.push(document.createTextNode(new_note + '\t'));
        }
        for(var num = 0 ; num < text.length; num++ ){
            li.appendChild(text[num]);
        }
        document.getElementById("recipe_ingredient_list").appendChild(li);
        document.getElementById("new_ingredient").value = "";
        document.getElementById("new_amount").value = "";
        document.getElementById("new_unit").value = "";
        document.getElementById("new_note").value = "";
    }


    var span = document.createElement("SPAN");
    var txt = document.createTextNode("\u00D7");
    span.className = "close";
    span.appendChild(txt);
    li.appendChild(span);

    for (i = 0; i < close.length; i++) {
        close[i].onclick = function () {
            var div = this.parentElement;
            div.style.display = "none";
        }
    }
}// Create a new list item when clicking on the "Add" button

// noinspection DuplicatedCode,DuplicatedCode,DuplicatedCode
function newElement3() {
    var li = document.createElement("li");
    var inputValue = document.getElementById("myInput3").value;
    var t = document.createTextNode(inputValue);
    li.appendChild(t);
    if (inputValue === '') {
        alert("You must write something!");
    } else {
        document.getElementById("myUL3").appendChild(li);
    }
    document.getElementById("myInput3").value = "";

    var span = document.createElement("SPAN");
    var txt = document.createTextNode("\u00D7");
    span.className = "close";
    span.appendChild(txt);
    li.appendChild(span);

    for (i = 0; i < close.length; i++) {
        close[i].onclick = function () {
            var div = this.parentElement;
            div.style.display = "none";
        }
    }
}

function openNav() {
    document.getElementById("sidenavID").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
    document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
}

function closeNav() {
    document.getElementById("sidenavID").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
    document.body.style.backgroundColor = "lightgrey";
}

var x = 0;

/**
 * read text input
 */
function readText(filePath) {
    var output = ""; //placeholder for text output
    //alert("hmm..."+filePath);
    if (filePath.files && filePath.files[0]) {
        reader.onload = function (e) {
            output = e.target.result;
            displayContents(output);
            testFoo();
        };//end onload()
        reader.readAsText(filePath.files[0]);
    }//end if html5 filelist support
    else if (ActiveXObject && filePath) { //fallback to IE 6-8 support via ActiveX
        try {
            reader = new ActiveXObject("Scripting.FileSystemObject");
            var file = reader.OpenTextFile(filePath, 1); //ActiveX File Object
            output = file.ReadAll(); //text contents of file
            file.Close(); //close file "input stream"
            displayContents(output);
        } catch (e) {
            if (e.number == -2146827859) {
                alert('Unable to access local files due to browser security settings. ' +
                    'To overcome this, go to Tools->Internet Options->Security->Custom Level. ' +
                    'Find the setting for "Initialize and script ActiveX controls not marked as safe" and change it to "Enable" or "Prompt"');
            }
        }
    } else { //this is where you could fallback to Java Applet, Flash or similar
        return false;
    }
    return true;
}

/**
 * display content using a basic HTML replacement
 */
function displayContents(txt) {
    //alert("displayContents");
    var el = document.getElementById('mainTA');
    el.innerHTML = txt; //display output in DOM
}

/**
 * Grab content from basic HTML replacement and use as json data
 */
function testFoo() {
    //alert("testFoo");

    var myData = document.getElementById('mainTA').value;

    var obj = JSON.parse(myData);

    document.getElementById('tableGoesHere').innerHTML = json2table(obj, 'table');

}

/***
 * use JSON data to create and fill html table
 * @param json
 * @param classes
 * @returns {string}
 */
function json2table(json, classes) {
    var cols = Object.keys(json[0]);

    var headerRow = '';
    var bodyRows = '';

    classes = classes || '';

    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    cols.map(function (col) {
        headerRow += '<th>' + capitalizeFirstLetter(col) + '</th>';
    });

    json.map(function (row) {
        bodyRows += '<tr>';

        cols.map(function (colName) {
            bodyRows += '<td>' + row[colName] + '</td>';
        });

        bodyRows += '</tr>';
    });

    return '<table class="' +
        classes +
        '"><thead><tr>' +
        headerRow +
        '</tr></thead><tbody>' +
        bodyRows +
        '</tbody></table>';
}

/**
 * Check for the various File API support.
 */
function checkFileAPI() {
    no_submit();
    if (window.File && window.FileReader && window.FileList && window.Blob) {
        reader = new FileReader();
        return true;
    } else {
        alert('The File APIs are not fully supported by your browser. Fallback required.');
        return false;
    }
}

/** ****************************************************
 *    FLASK STUFF
 *    # = id
 *    . = class
 *    nothing = tag
 *******************************************************/

function init_db(){
    console.log("init_db from main js initiated.");
    $.ajax({
        type: "GET",
        url: "http://localhost:5000/init_db",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (res) {
            console.log(res);
        },
        failure: function (err) {
            console.log(err);
        }
    });
}

function no_submit() {
    $("#myform").on("submit", function (event) {
        event.preventDefault();
        var data = {"name": $('[name="fieldname"]').val()}
        console.log(data);
        $.ajax({
            type: "POST",
            url: "http://localhost:5000/foo",
            // The key needs to match your method's input parameter (case-sensitive).
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (res) {
                console.log(res);
            },
            failure: function (err) {
                console.log(err);
            }
        });
    });
}

function load_cabinet() {
    console.log("load-cabinet")
    $.ajax({
        type: "GET",
        url: "http://localhost:5000/get_ingredients",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (res) {
            var tbl = json_ingredient_table(res, 'table')
            $('#cabinets_ta').html(tbl);
        },
        failure: function (err) {
            console.log(err);
        }
    });
}

function load_Recipes() {
    console.log("load-recipes")
    $.ajax({
        type: "GET",
        url: "http://localhost:5000/get_recipes",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (res) {
            var tbl = json_recipe_table(res, 'table')
            $('#cook_book_ta').html(tbl);
        },
        failure: function (err) {
            console.log(err);
        }
    });
}

/***
 * use JSON data to create and fill html table
 * @param json
 * @param classes
 * @returns {string}
 */
function json_recipe_table(json, classes) {
    var cols = Object.keys(json[0]);

    var headerRow = '';
    var bodyRows = '';
    var cb = "";
    var traits = "type=\"button\" class=\"ok\" onclick=\"foo()\" value=\"Submit\"";
    classes = classes || '';

    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }


    json.map(function (row) {
        bodyRows += '<tr>';
        cols.map(function (colName) {
            bodyRows += '<td>' + row[colName] + '</td>';
            cb = row[colName];
        });
        bodyRows += '<td><input class="ok_box" type="checkbox" id=' + cb + '></td>';
    });


    return '<table id="table" class="' +
        classes +
        '"><tbody>' +
        bodyRows +
        '</tbody></table>';
}

/***
 * use JSON data to create and fill html table
 * @param json
 * @param classes
 * @returns {string}
 */
function json_ingredient_table(json, classes) {
    var cols = Object.keys(json[0]);

    var headerRow = '';
    var bodyRows = '';
    var cb = "";
    var traits = "type=\"button\" class=\"ok\" onclick=\"foo()\" value=\"Submit\"";

    classes = classes || '';

    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    json.map(function (row) {
        bodyRows += '<tr>';

        cols.map(function (colName) {
            // if(row[colName] > 0){
            //
            // }else{
            bodyRows += '<td>' + row[colName] + '</td>';
            cb = row[colName];
            // }
        });
        bodyRows += '<td><input class="ok_box" type="checkbox" id=' + cb + '></td>';

        bodyRows += '</tr>';
    });

    return '<table id="table" class="' +
        classes +
        '"><tbody>' +
        bodyRows +
        '</tbody></table>';
}

/**
 *
 * **/
function add_item(num) {
    console.log(`add_item called from main.js ${num}`)
}

/**
 *

 function add_recipe(num){
    console.log(`add_recipe called from main.js ${num}`)
}* **/

/**
 *
 *
 function send_to_cabinet(num){
    console.log(`send_to_cabinet called from main.js ${num}`)
}**/

/**
 *
 ***/
 function send_to_cook_book(){
    console.log(`send_to_cook_book called from main.js `);
    alert("send 2 cb pushed");


}

function get_recipe_form(count) {
    // var formRows = '';
    // var form_hdr_label = '<th colspan="2">Recipe Name : </th>';
    //
    // var form_hdr_start = '<th colspan="4"><input type="text" name="RecipeName" ' +
    //     'placeholder="Enter Recipe Name..." autocomplete="off"></th>';
    //
    // var row_start = '<td><input type="text" name=" ';
    // var row_mid = ' " placeholder="Enter ';
    // var row_end = '" autocomplete="off"></td>';
    //
    //
    // var names = ["recipe", "ingredient", "amount", "unit", "note"];
    //
    // //for (var i = 0; i < count; i++) {
    //     names.forEach(func);
    // //}
    //
    // var strings = '';
    //
    // var append_strings ='<tr>';
    // function func(item, index) {
    //     console.log("function called ");
    //     strings = '<tr>'
    //     // if (index == 0) {
    //     //     console.log("if index :("+ index+ ") strings: ["+strings+"]");
    //     // } else {
    //         strings += row_start + item + row_mid + item + row_end;
    //         console.log("else index :("+ index+ ") strings: ["+strings+"]");
    //     // }
    //     strings += '</tr>';
    //     append_strings += strings;
    //     console.log("append strings ["+ append_strings+"]")
    // }
    //
    // var tbl = '<table class="table"><tr>' + form_hdr_label +
    //     form_hdr_start +'</tr>' + append_strings + '</table>';

    var tbl = '<div id="leftDiv" class="header" style="height: 10vh;">' +
        '    <h2 style="float: left">Add Recipe</h2>' +
        '    <ul></ul><input type="text" id="recipe_name" placeholder="Recipe name..."><br>' +
        '    <input type="text" id="myInput2" placeholder="Ingredient..">' +
        '    <span onclick="add_ingredient_to_recipe()" class="addBtn">Add</span>' +
        '  </div>' +
        '  <ul id="myUL2">' +
        '  </ul>';
    console.log("Table [ " + tbl + "]");

    //var tbl = json_recipe_table(res, 'table')
    $('#add_recipe_ta').html(tbl);
}

/***
 * use JSON data to create and fill html table
 * @param json
 * @param classes
 * @returns {string}
 */
function json_recipe_table(json, classes) {
    var cols = Object.keys(json[0]);

    var headerRow = '';
    var bodyRows = '';
    var cb = "";
    var traits = "type=\"button\" class=\"ok\" onclick=\"foo()\" value=\"Submit\"";
    classes = classes || '';

    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }


    json.map(function (row) {
        bodyRows += '<tr>';
        cols.map(function (colName) {
            bodyRows += '<td>' + row[colName] + '</td>';
            cb = row[colName];
        });
        bodyRows += '<td><input class="ok_box" type="checkbox" id=' + cb + '></td>';
    });


    return '<table id="table" class="' +
        classes +
        '"><tbody>' +
        bodyRows +
        '</tbody></table>';
}

function load_recipe_file(filePath){

   var output = ""; //placeholder for text output
    //alert("hmm..."+filePath);
    if (filePath.files && filePath.files[0]) {
       // alert("got a path still...");
        reader.onload = function (e) {
            output = e.target.result;
            displayRecipeContents(output);
            //recipeFoo();
        };//end onload()
        reader.readAsText(filePath.files[0]);
    }//end if html5 filelist support
    else if (ActiveXObject && filePath) { //fallback to IE 6-8 support via ActiveX
        try {
            alert("old shit");
            reader = new ActiveXObject("Scripting.FileSystemObject");
            var file = reader.OpenTextFile(filePath, 1); //ActiveX File Object
            output = file.ReadAll(); //text contents of file
            file.Close(); //close file "input stream"
            displayRecipeContents(output);
        } catch (e) {
            if (e.number == -2146827859) {
                alert('Unable to access local files due to browser security settings. ' +
                    'To overcome this, go to Tools->Internet Options->Security->Custom Level. ' +
                    'Find the setting for "Initialize and script ActiveX controls not marked as safe" and change it to "Enable" or "Prompt"');
            }
        }
    } else { //this is where you could fallback to Java Applet, Flash or similar
        return false;
    }
    return true;
}


/**
 * display content using a basic HTML replacement
 */
function displayRecipeContents(txt) {
    //alert("displayRecipeContents ");
    var el = document.getElementById('recipe_ingredient_list');
    console.log("txt = " + typeof txt);
    var jsonObj = JSON.parse(txt);

    console.log("jsonObj = " + typeof jsonObj);

    console.log(typeof txt);
    el.innerHTML = json2table(jsonObj, 'table'); //display output in DOM
}

/**
 * Grab content from basic HTML replacement and use as json data
 */
function recipeFoo() {
    alert("recipeFoo");

    // var myData = document.getElementById('recipe_ingredient_list').value;
    // console.log(typeof myData);
    //
    // var obj = JSON.parse(myData);
    // console.log(typeof obj);
    //
    // document.getElementById('add_recipe_ta').innerHTML = json2table(obj, 'table');

}
