# GLMkr

#GLMkr Project Intent and Goals

The main intent of the GLMkr project is to provide a browser based 'cook book' repository of recipes for the user. 


#GLMkr Project Concept for GLMkr

Using a common web browser, the user will be able to load and save recipes and their ingredients. 
The user can use this 'cook book' to review the required ingredients for each recipe.

#GLMkr Project Technical Profile

Required plug-ins, languages, and frameworks.
* basic web browser
* python
* cors
* flask-alchemy
* sqlite3

#Initial Start-Up

To initialize the app:
 
* download and install the required plug-ins, languages, and frameworks
* within the installed folder, run 'python ~/glmkr/python/flaskRouter.py' from terminal
* from a web browser of choice go to 'http://localhost:5000/html/homePage.html'
* click 'Initialize' 

The app should now be ready for use. 




