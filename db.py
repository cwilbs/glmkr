import os
import sqlite3
import json

# create a default path to connect to and create (if necessary) a database
# called 'database.sqlite3' in the same directory as this script
DEFAULT_PATH = "/Users/chris.a.wilbur/PhpstormProjects/glmkr/data/data.db"


def db_connect(db_path=DEFAULT_PATH):
    con = sqlite3.connect(db_path)
    return con


def create_db():
    con = db_connect()
    cur = con.cursor()
    with open('/Users/chris.a.wilbur/PhpstormProjects/glmkr/data/CREATE_DB.txt', 'r') as script:
        commands = script.read().strip().split(";")
        for command in commands:
            cur.execute(command.strip() + ";")
    con.commit()
    con.close()


def insert_into_ingredient_table(cur, name, unit, amount, note):
    sql = "INSERT into INGREDIENT (name, unit, amount, note) \
                 VALUES(?,?,?,?);"
    return cur.execute(sql, (name, amount, unit, note))


def insert_into_recipe_table(cur, name):
    sql = "INSERT into RECIPE (name) VALUES (?);"
    return cur.execute(sql, (name,))


def insert_into_recipe_ingredient_table(cur, recipe_name, ingredient_name, unit, amount, note):
    sql = "INSERT into RecipeToIngredient (recipe, ingredient, unit, amount, note) \
                 VALUES (?,?,?,?,?);"
    return cur.execute(sql, (recipe_name, ingredient_name, unit, amount, note))


def seed_ingredients():
    con = db_connect()
    cur = con.cursor()
    with open("/Users/chris.a.wilbur/PhpstormProjects/glmkr/data/test_ingredients.json", 'r') as source:
        ingredients = json.loads(source.read())
        for ingredient in ingredients:
            insert_into_ingredient_table(cur, ingredient["ingredient"], ingredient["amount"], ingredient["unit"], "")
    con.commit()


def seed_recipes():
    con = db_connect()
    cur = con.cursor()
    with open("/Users/chris.a.wilbur/PhpstormProjects/glmkr/data/test_recipes.json", 'r') as source:
        recipes = json.loads(source.read())
        for recipe in recipes:
            insert_into_recipe_table(cur, recipe["name"])
    con.commit()


def seed_recipe_ingredients():
    con = db_connect()
    cur = con.cursor()
    with open("/Users/chris.a.wilbur/PhpstormProjects/glmkr/data/test_recipe_ingredients.json", 'r') as source:
        ris = json.loads(source.read())
        for ri in ris:
            insert_into_recipe_ingredient_table(cur, ri["recipe"], ri["ingredient"],
                                                ri["unit"], ri["amount"], ri["note"])

    con.commit()


def get_ingredients_for_recipe(recipe_name=""):
    if not recipe_name:
        return []
    con = db_connect()
    cur = con.cursor()

    query = cur.execute("Select ingredient, amount, unit, note from RecipeToIngredient where recipe = ?",
                        (recipe_name,))
    return query.fetchall()


def get_recipes_by_ingredient(ingredient_name=""):
    if not ingredient_name:
        return []
    con = db_connect()
    cur = con.cursor()

    query = cur.execute("Select recipe from RecipeToIngredient where ingredient = ?", (ingredient_name,))
    return query.fetchall()


# create_db()
# seed_recipes()
# seed_ingredients()
# seed_recipe_ingredients()
print(get_ingredients_for_recipe("Spicy Ball Park Nachos"))
print(get_recipes_by_ingredient("Ground Beef"))
