import os
import sqlite3
import json

# create a default path to connect to and create (if necessary) a database
# called 'database.sqlite3' in the same directory as this script
DEFAULT_PATH = "/Users/chris.a.wilbur/PhpstormProjects/glmkr/data/data.db"


def db_connect(db_path=DEFAULT_PATH):
    con = sqlite3.connect(db_path)
    return con


def create_db():
    con = db_connect()
    cur = con.cursor()
    with open('/Users/chris.a.wilbur/PhpstormProjects/glmkr/data/CREATE_DB.txt', 'r') as script:
        commands = script.read().split(';')
        for command in commands:
            cur.execute(command + ";")
    con.commit()
    con.close()


def insert_into_ingredient_table(cur, name, unit, amount, note):
    print("insert_into_ingredient_table")
    sql = "INSERT OR IGNORE into INGREDIENT (name, unit, amount, note) \
                 VALUES(?,?,?,?);"
    return cur.execute(sql, (name, amount, unit, note))


def insert_into_recipe_table(cur, name):
    print("insert_into_recipe_table")
    sql = "INSERT OR IGNORE into RECIPE (name) VALUES (?);"
    return cur.execute(sql, (name,))


def insert_into_recipe_ingredient_table(cur, recipe_name, ingredient_name, unit, amount, note):
    print("insert_into_recipe_ingredient_table")
    sql = "INSERT OR IGNORE into RecipeToIngredient (recipe, ingredient, unit, amount, note) \
                 VALUES (?,?,?,?,?);"
    return cur.execute(sql, (recipe_name, ingredient_name, unit, amount, note))


def seed_ingredients(path="/Users/chris.a.wilbur/PhpstormProjects/glmkr/data/test_ingredients.json"):
    print("seed_ingredients")
    con = db_connect()
    cur = con.cursor()
    with open(path, 'r') as source:
        ingredients = json.loads(source.read())
        for ingredient in ingredients:
            insert_into_ingredient_table(cur, ingredient["ingredient"], ingredient["amount"], ingredient["unit"], "")
    con.commit()


def seed_recipes(path="/Users/chris.a.wilbur/PhpstormProjects/glmkr/data/test_recipes.json"):
    print("seed_recipes")
    con = db_connect()
    cur = con.cursor()
    with open(path, 'r') as source:
        recipes = json.loads(source.read())
        for recipe in recipes:
            insert_into_recipe_table(cur, recipe["name"])
    con.commit()


def seed_recipe_ingredients(path="/Users/chris.a.wilbur/PhpstormProjects/glmkr/data/test_recipe_ingredients.json"):
    print("seed_recipe_ingredients")
    con = db_connect()
    cur = con.cursor()
    with open(path, 'r') as source:
        ris = json.loads(source.read())
        for ri in ris:
            insert_into_recipe_ingredient_table(cur, ri["recipe"], ri["ingredient"],
                                                ri["unit"], ri["amount"], ri["note"])
    con.commit()


def insert_json_to_all_tables(path="/Users/chris.a.wilbur/PhpstormProjects/glmkr/data/taco_soup.json"):
    con = db_connect()
    cur = con.cursor()
    with open(path, 'r') as source:
        ris = json.loads(source.read())
        for ri in ris:
            insert_into_recipe_table(cur, ri["recipe"])
            insert_into_recipe_ingredient_table(cur, ri["recipe"], ri["ingredient"],
                                                ri["unit"], ri["amount"], ri["note"])
            insert_into_ingredient_table(cur, ri["ingredient"], ri["amount"], ri["unit"], ri["note"])
    con.commit()


def insert_string_to_all_tables(data):
    con = db_connect()
    cur = con.cursor()
    ris = json.loads(data)
    for ri in ris:
        insert_into_recipe_table(cur, ri["recipe"])
        insert_into_recipe_ingredient_table(cur, ri["recipe"], ri["ingredient"],
                                            ri["unit"], ri["amount"], ri["note"])
        insert_into_ingredient_table(cur, ri["ingredient"], ri["amount"], ri["unit"], ri["note"])

    con.commit()


def add_recipe(name, ingredients):
    con = db_connect()
    cur = con.cursor()
    insert_into_ingredient_table(cur, name, unit, amount)
    con.commit()


def add_ingredient(name, unit="", amount=""):
    con = db_connect()
    cur = con.cursor()
    insert_into_ingredient_table(cur, name, unit, amount)
    con.commit()


def get_ingredients():
    con = db_connect()
    cur = con.cursor()
    cur.execute(""" SELECT count(name) FROM sqlite_master WHERE type='table' AND name='Ingredient' """)
    if cur.fetchone()[0] == 1:
        cur.execute(
            "SELECT * from Ingredient"
        )
        return cur.fetchall()
    else:
        return "please initialize system"


def get_recipes():
    con = db_connect()
    cur = con.cursor()
    cur.execute(""" SELECT count(name) FROM sqlite_master WHERE type='table' AND name='Recipe' """)
    if cur.fetchone()[0] == 1:
        cur.execute(
            "SELECT * from Recipe"
        )
        return cur.fetchall()
    else:
        return "please initialize system"


def get_ingredients_for_recipe(recipe_name=""):
    if not recipe_name:
        return []
    con = db_connect()
    cur = con.cursor()
    cur.execute(""" SELECT count(name) FROM sqlite_master WHERE type='table' AND name='RecipeToIngredient' """)
    if cur.fetchone()[0] ==1:
        query = cur.execute("Select ingredient, amount, unit, note from RecipeToIngredient where recipe = ?", (recipe_name,))
        return query.fetchall()
    else:
        return "please initialize system"

    # query = cur.execute("Select ingredient, amount, unit, note from RecipeToIngredient where recipe = ?",
    #                     (recipe_name,))
    # return query.fetchall()


def get_recipes_by_ingredient(ingredient_name=""):
    if not ingredient_name:
        return []
    con = db_connect()
    cur = con.cursor()

    query = cur.execute("Select recipe from RecipeToIngredient where ingredient = ?", (ingredient_name,))
    return query.fetchall()


def init_db():
    create_db()
    seed_recipes()
    seed_ingredients()
    seed_recipe_ingredients()


jsonString = [
    {"recipe": "Taco Soup", "ingredient": "Ranch salad dressing mix", "unit": "package", "amount": "1", "note": ""},
    {"recipe": "Taco Soup", "ingredient": "Taco Seasoning Mix", "unit": "package", "amount": "1", "note": ""},
    {"recipe": "Taco Soup", "ingredient": "Green Olives", "unit": "cup", "amount": "0.5", "note": ""},
    {"recipe": "Taco Soup", "ingredient": "Black Olives", "unit": "can", "amount": "1", "note": "sliced"},
    {"recipe": "Taco Soup", "ingredient": "Green Chiles", "unit": "can", "amount": "2", "note": "diced"},
    {"recipe": "Taco Soup", "ingredient": "Tomatoes", "unit": "can", "amount": "1", "note": "diced"},
    {"recipe": "Taco Soup", "ingredient": "Tomatoes with chilies", "unit": "can", "amount": "1", "note": ""},
    {"recipe": "Taco Soup", "ingredient": "Mexican Tomatoes", "unit": "can", "amount": "1", "note": "drained"},
    {"recipe": "Taco Soup", "ingredient": "Whole Kernal Corn", "unit": "can", "amount": "1", "note": ""},
    {"recipe": "Taco Soup", "ingredient": "Pink Kidney Beans", "unit": "can", "amount": "1", "note": ""},
    {"recipe": "Taco Soup", "ingredient": "Pinto Beans", "unit": "can", "amount": "2", "note": ""},
    {"recipe": "Taco Soup", "ingredient": "Onions", "unit": "cup", "amount": "2", "note": ""},
    {"recipe": "Taco Soup", "ingredient": "Ground Beef or Chicken", "unit": "Lb", "amount": "2", "note": ""},
    {"recipe": "Taco Soup", "ingredient": "Jalapeno", "unit": "", "amount": "", "note": ""},
    {"recipe": "Taco Soup", "ingredient": "Grated Cheese", "unit": "", "amount": "", "note": ""},
    {"recipe": "Taco Soup", "ingredient": "Sour Cream", "unit": "", "amount": "", "note": ""},
    {"recipe": "Taco Soup", "ingredient": "Corn Chips", "unit": "", "amount": "", "note": ""},
    {"recipe": "Taco Soup", "ingredient": "Green Onions", "unit": "", "amount": "", "note": ""}
]

create_db()
seed_recipes()
seed_ingredients()
seed_recipe_ingredients()
# insert_json_to_all_tables()
# Since Taco Soup is not there yet, it will return an empty string.
print(get_ingredients_for_recipe("Taco Soup"))
print("")

print(get_ingredients_for_recipe("Spicy Ball Park Nachos"))
print("")
print(get_recipes_by_ingredient("Ground Beef"))

# insert_json_to_all_tables()
# insert_json_to_all_tables("/Users/chris.a.wilbur/PhpstormProjects/glmkr/data/test_recipe_ingredients.json")
# product_sql = "INSERT INTO products (name, price) VALUES (?, ?)"
# cur.execute(product_sql, ('Introduction to Combinatorics', 7.99))
# cur.execute(product_sql, ('A Guide to Writing Short Stories', 17.99))
# cur.execute(product_sql, ('Data Structures and Algorithms', 11.99))
# cur.execute(product_sql, ('Advanced Set Theory', 16.99))
# insert_string_to_all_tables(json.dumps(jsonString))
# print json.dumps(get_ingredients())
