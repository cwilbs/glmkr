# Importing flask module in the project is mandatory
# An object of Flask class is our WSGI application. 
from flask import Flask, request, jsonify, Response, send_from_directory
from flask_cors import CORS
import first
import json

# Flask constructor takes the name of  
# current module (__name__) as argument. 
app = Flask(__name__, static_url_path='')


@app.route('/js/<path:path>')
def send_css(path):
    return send_from_directory('/Users/chris.a.wilbur/PhpstormProjects/glmkr/js', path)


@app.route('/css/<path:path>')
def send_js(path):
    return send_from_directory('/Users/chris.a.wilbur/PhpstormProjects/glmkr/css', path)


@app.route('/html/<path:path>')
def send_html(path):
    return send_from_directory('/Users/chris.a.wilbur/PhpstormProjects/glmkr/html', path)


@app.route('/')
def send_home():
    return send_from_directory('/Users/chris.a.wilbur/PhpstormProjects/glmkr/html/homePage.html')


app.config['SECRET_KEY'] = 'the quick brown fox jumps over the lazy   dog'
app.config['CORS_HEADERS'] = 'Content-Type'

cors = CORS(app, resources={r"/foo": {"origins": "http://localhost:port"}})


# The route() function of the Flask class is a decorator,
# which tells the application which URL should call  
# the associated function. 
@app.route('/hello_world')
# ‘/’ URL is bound with hello_world() function. 
def hello_world():
    return 'Hello World...'


@app.route('/init_db')
def init_db():
    print("init db from flaskRouter.py")
    first.init_db()
    #response = Response(200)
    return 'got it'


@app.route('/Nothello_world/<greeting>')
# ‘/’ URL is bound with hello_world() function.
def Nothello_world(greeting=None):
    # first.add_ingredient(greeting)
    return "added " + greeting


@app.route('/foo', methods=['POST'])
def foo():
    data = request.json
    jasondata = data
    try:
        first.add_ingredient(jasondata["name"])
        return Response(json.dumps(data), 200)
    except:
        return Response("ERROR: ingredient already exists", 500)


@app.route('/handle_data', methods=['POST'])
def handle_data():
    projectpath = request.form['fieldname']
    response = Response(200)
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/get_ingredients', methods=['GET'])
def get_ingredients():
    # print("get_ingredients")
    data = first.get_ingredients()
    try:
        return json.dumps(data)
    except:
        return Response("ERROR!: initialize system")


@app.route('/get_recipes', methods=['GET'])
def get_recipes():
    # print("get_recipes")
    data = first.get_recipes()
    try:
        return json.dumps(data)
    except:
        return Response("OOPS!!")


@app.route('/add_item', methods=['POST'])
def add_item():
    return 'add_item'


@app.route('/send_to_cook_book/<path:path>')
def send_to_cook_book():
    print("send_to_cook_book from flaskRouter.py")
    first.send_to_cook_book()
    #response = Response(200)
    return 'Recipe loaded'


# main driver function
if __name__ == '__main__':
    # run() method of Flask class runs the application
    # on the local development server.
    app.run('localhost')
